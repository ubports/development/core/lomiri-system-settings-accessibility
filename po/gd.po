# Gaelic; Scottish translation for lomiri-system-settings
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-accessibility package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
# Michael Bauer <fios@akerbeltz.org>, 2014.
# GunChleoc <fios@foramnagaidhlig.net>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-accessibility\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2014-10-20 19:38+0000\n"
"Last-Translator: GunChleoc <Unknown>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:38
msgid "Accessibility"
msgstr "Accessibility;So-inntrigeachd"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:40
msgid "accessibility"
msgstr "accessibility;so-inntrigeachd"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:42
msgid "a11y"
msgstr "a11y"

