# Welsh translation for lomiri-system-settings-accessibility
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lomiri-system-settings-accessibility package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-accessibility\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2015-07-14 12:20+0000\n"
"Last-Translator: Owen Llywelyn <owen.llywelyn@gmail.com>\n"
"Language-Team: Welsh <cy@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : (n != 8 && n != 11) ? "
"2 : 3;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:42+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:38
msgid "Accessibility"
msgstr "Hygyrchedd"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:40
msgid "accessibility"
msgstr "hygyrchedd"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:42
msgid "a11y"
msgstr "a11y"

