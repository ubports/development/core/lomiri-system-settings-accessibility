# Dutch translation for lomiri-system-settings-accessibility
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-accessibility package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-accessibility\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2021-03-21 15:22+0000\n"
"Last-Translator: Heimen Stoffels <vistausss@outlook.com>\n"
"Language-Team: Dutch <https://translate.ubports.com/projects/ubports/"
"system-settings/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:38
msgid "Accessibility"
msgstr "Toegankelijkheid"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:40
msgid "accessibility"
msgstr "toegankelijkheid"

#. TRANSLATORS: This is a keyword or name for the accessibility plugin which is used while searching
#: ../build/po/settings.js:42
msgid "a11y"
msgstr "a11y"

